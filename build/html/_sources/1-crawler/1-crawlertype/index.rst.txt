爬虫分类
=========================

根据使用场景，网络爬虫可分为:

.. toctree::
   :titlesonly:
   :glob:


   1-commonCrawler/index
   2-focusCrawler/index
