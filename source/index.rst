.. PythonCrawler documentation master file, created by
   sphinx-quickstart on Sun Aug 05 10:24:49 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PythonCrawler's documentation!
=========================================

大数据时代，数据从何而来？

- 企业产生的用户数据：\ `百度指数 <http://index.baidu.com/>`_\ 、\ `阿里指数 <https://alizs.taobao.com/>`_\ 、\ `TBI腾讯浏览指数 <http://tbi.tencent.com/>`_\ 、\ `新浪微博指数 <http://data.weibo.com/index>`_\ 
- 数据平台购买数据：\ `数据堂 <http://www.datatang.com/about/about-us.html>`_\ 、\ `国云数据市场 <http://www.moojnn.com/data-market/>`_\ 、\ `贵阳大数据交易所 <http://trade.gbdex.com/trade.web/index.jsp>`_\ 
- 政府/机构公开的数据：\ `中华人民共和国国家统计局数据 <http://data.stats.gov.cn/index.htm>`_\ 、\ `世界银行公开数据 <http://data.worldbank.org.cn/>`_\ 、\ `联合国数据 <http://data.un.org/>`_\ 、\ `纳斯达克 <http://www.nasdaq.com/zh>`_\ 
- 数据管理咨询公司：\ `麦肯锡 <http://www.mckinsey.com.cn/>`_\ 、\ `埃森哲 <https://www.accenture.com/cn-zh/>`_\ 、\ `艾瑞咨询 <http://www.iresearch.com.cn/>`_\ 
- 爬取网络数据：\ `爬虫工程师 <http://baike.baidu.com/link?url=3SeA6RcStie6o9T5XGMTBoHK-BGiO_0IqtnI4IkVjDBsoKrJL0aotjA4cjqbx8wF>`_\ 

作为一名python爬虫工程师，我们需要掌握的内容有

.. toctree::
   :titlesonly:
   :glob:


   1-crawler/index
   2-datagrab/index
   3-scrapy/index
   4-redis/index
