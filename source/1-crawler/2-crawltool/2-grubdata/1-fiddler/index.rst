fiddler
=========================

\ ``Fiddler``\ 是一款强大Web调试工具，它能记录所有客户端和服务器的HTTP请求；Fiddler启动的时候，默认代理设为了IE代理\ ``127.0.0.1:8888``\ ，而其他浏览器是需要手动设置

- \ `工作原理 <#princle>`_\ 
- \ `基本设置 <#setting>`_\ 
- \ `开启代理 <#proxyl>`_\ 

	- \ `设置chrome代理 <#chromel>`_\ 
- \ `信息解读 <#getinfo>`_\ 



.. _princle:

0x00 工作原理
~~~~~~~~~~~~~~~~~~~~~~~~~

\ ``Fiddler``\ 是以代理web服务器的形式工作的，它使用代理地址：\ ``127.0.0.1``\ ，端口：\ ``8888``\ 

.. figure:: ../images/1.png

.. _setting:

0x01 基本设置
~~~~~~~~~~~~~~~~~~~~~~~~~

- 启动\ ``Fiddler``\ ，打开菜单栏中的\ ``Tools->Options``\ ，打开\ ``Fiddler Options``\ 对话框

.. figure:: ../images/2.png

- 对\ ``Fiddler``\ 进行相关选项的设置

	- 打开\ ``工具栏->Tools->Fiddler Options->HTTPS``\ 
	- 选中\ ``Capture HTTPS CONNECTs(捕捉HTTPS连接)``\ 
	- 选中\ ``Decrypt HTTPS traffic(解密HTTPS通信)``\ 
	- 如果要用Fiddler获取本机所有进程的HTTPS请求，需要在中间的下拉菜单中选中\ ``...from all processes(从所有进程)``\ 
	- 选中下方\ ``Ignore server certificate errors(忽略服务器证书错误)``\ 

.. figure:: ../images/3.png

- 为\ ``Fiddler``\ 配置Windows信任这个根证书解决安全警告：\ ``Trust Root Certificate(受信任的根证书)``\ 

.. figure:: ../images/4.png

- 打开\ ``Fiddler``\ ，选中\ ``主菜单Tools->Fiddler Options…->Connections``\ 

	- 选中\ ``Allow remote computers to connect(允许远程连接)``\ 
	- 选中\ ``Act as system proxy on startup(作为系统启动代理)``\ 

.. figure:: ../images/5.png

- 重启\ ``Fiddler``\ ，使配置生效(这一步很重要，必须做)

.. _proxyl:

0x02 开启代理
~~~~~~~~~~~~~~~~~~~~~~~~~

\ ``Fiddler``\ 启动的时候，默认的代理设置为IE代理\ ``127.0.0.1:8888``\ ，所以IE浏览器无需手动设置就可以使用\ ``Fiddler代理``\ ；而其它浏览器是需要手动设置才可以使用\ ``Fiddler代理``\ 

.. _chromel:

0x0000 设置chrome代理
~~~~~~~~~~~~~~~~~~~~~~~

- 安装\ ``SwitchyOmega``\ 代理Chrome浏览器插件

.. figure:: ../images/6.png

- 如图所示，设置代理服务器为\ ``127.0.0.1:8888``\ 

.. figure:: ../images/7.png

- 通过浏览器插件切换为设置好的代理

.. figure:: ../images/8.png


.. _getinfo:

0x03 信息解读
~~~~~~~~~~~~~~~~~~~~~~~~~

设置好后，本机经过\ ``127.0.0.1:8888``\ 代理的所有HTTP通信，都会被Fiddler拦截到

.. figure:: ../images/9.png

如上图所示，信息解读窗口主要由以下几部分组成

- \ ``会话窗口``\ ：记录所有的HTTP通信会话信息
- \ ``HTTP Request请求窗口``\ ：显示单个\ ``HTTP Request``\ 请求信息

	- \ ``Headers``\ ：显示客户端发送到服务器的HTTP请求的header，显示为一个分级视图，包含了\ ``Web客户端信息``\ 、\ ``Cookie``\ 、\ ``传输状态``\ 等
	- \ ``Textview``\ ：显示POST请求的body部分为文本
	- \ ``WebForms``\ ：显示请求的GET参数和\ ``POST body``\ 内容
	- \ ``HexView``\  ：用十六进制数据显示请求
	- \ ``Auth``\ ：显示响应header中的\ ``Proxy-Authorization(代理身份验证)``\ 和\ ``Authorization(授权)``\ 信息
	- \ ``Raw``\ ：将整个请求显示为纯文本
	- \ ``JSON``\ ：显示JSON格式文件
	- \ ``XML``\ ：如果请求的body是XML格式，就是用分级的XML树来显示它

- \ ``HTTP Response响应窗口``\ ：显示单个\ ``HTTP Response``\ 响应信息

	- \ ``Transformer``\ ：显示响应的编码信息
	- \ ``Headers``\ ：用分级视图显示响应的 header
	- \ ``TextView``\ ：使用文本显示相应的 body
	- \ ``ImageVies``\ ：如果请求是图片资源，显示响应的图片
	- \ ``HexView``\ ：用十六进制数据显示响应
	- \ ``WebView``\ ：响应在Web浏览器中的预览效果
	- \ ``Auth``\ ：显示响应header中的\ ``Proxy-Authorization(代理身份验证)``\ 和\ ``Authorization(授权)``\ 信息
	- \ ``Caching``\ ：显示此请求的缓存信息
	- \ ``Privacy``\ ：显示此请求的私密 (P3P) 信息
	- \ ``Raw``\ ：将整个响应显示为纯文本
	- \ ``JSON``\ ：显示JSON格式文件
	- \ ``XML``\ ：如果响应的body是XML格式，就是用分级的XML树来显示它
