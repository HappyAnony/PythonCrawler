urllib2模块
=========================

\ ``urllib2``\ 是\ ``Python2.7``\ 官方自带的模块，不需要下载，导入即可使用

参考文档：

- \ `urllib2官方文档 <https://docs.python.org/2/library/urllib2.html>`_\ 
- \ `urllib2官方源码 <https://hg.python.org/cpython/file/2.7/Lib/urllib2.py>`_\ (\ **注意：该源码还可在python27安装根目录下的Lib目录中找到**\ )

